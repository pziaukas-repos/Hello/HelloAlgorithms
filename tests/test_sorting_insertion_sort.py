import sorting
import tests
import unittest


class InsertionSortTest(unittest.TestCase):
    def setUp(self):
        self.pile = tests.Pile(100)

    def test_insertion_sort(self):
        self.assertEqual(sorting.insertion_sort(self.pile.raw), self.pile.sorted)


if __name__ == "__main__":
    unittest.main()
