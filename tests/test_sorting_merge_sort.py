import sorting
import tests
import unittest


class MergeSortTest(unittest.TestCase):
    def setUp(self):
        self.pile = tests.Pile(100)

    def test_merge(self):
        double_pile = tests.Pile()
        double_pile.raw = 2 * self.pile.sorted
        sorting.merge(double_pile.raw, 0, len(double_pile) // 2, len(double_pile))
        self.assertEqual(double_pile.raw, double_pile.sorted)

    def test_merge_sort(self):
        sorting.merge_sort(self.pile.raw)
        self.assertEqual(self.pile.raw, self.pile.sorted)


if __name__ == "__main__":
    unittest.main()
