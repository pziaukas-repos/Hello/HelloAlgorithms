"""
Toy module for algorithm testing
"""
import random
from typing import List

random.seed(9001)


class Pile:
    """
    A straightforward pile implementation.
    """
    def __init__(self, n: int = 10) -> None:
        """
        Pile constructor.
        :param n: Length of a pile.
        """
        self.raw = [random.randint(0, n) for _ in range(n)]

    @property
    def sorted(self) -> List[int]:
        return sorted(self.raw)

    def __len__(self) -> int:
        return len(self.raw)
