import sorting
import tests
import unittest


class QuickSortTest(unittest.TestCase):
    def setUp(self):
        self.pile = tests.Pile(100)

    def test_partition(self):
        sequence = [2, 8, 7, 1, 3, 5, 6, 4]
        q = sorting.partition(sequence, 0, len(sequence))
        self.assertEqual(q, 3)
        self.assertEqual(sequence, [2, 1, 3, 4, 7, 5, 6, 8])

    def test_quick_sort(self):
        sorting.quick_sort(self.pile.raw)
        self.assertEqual(self.pile.raw, self.pile.sorted)

    def test_quick_sort(self):
        sorting.quick_sort(self.pile.raw, randomized=True)
        self.assertEqual(self.pile.raw, self.pile.sorted)


if __name__ == "__main__":
    unittest.main()
