import math
import sorting
import tests
import unittest


class BucketSortTest(unittest.TestCase):
    def setUp(self):
        self.pile = tests.Pile(1000)

    def test_bucket_sort_log2(self):
        bucket_count = round(math.log2(len(self.pile)))
        self.assertEqual(sorting.bucket_sort(self.pile.raw, bucket_count), self.pile.sorted)

    def test_bucket_sort_log10(self):
        bucket_count = round(math.log10(len(self.pile)))
        self.assertEqual(sorting.bucket_sort(self.pile.raw, bucket_count), self.pile.sorted)


if __name__ == "__main__":
    unittest.main()
