import math
import sorting
import tests
import unittest


class RadixSortTest(unittest.TestCase):
    def setUp(self):
        self.pile = tests.Pile(1000)

    def test_counting_sort_base_10(self):
        # base-10 by default
        digits = len(str(max(self.pile.raw)))
        sorting.radix_sort(self.pile.raw, digits)
        self.assertEqual(self.pile.raw, self.pile.sorted)

    def test_counting_sort_base_2(self):
        digits = math.floor(math.log(max(self.pile.raw), 2)) + 1
        sorting.radix_sort(self.pile.raw, digits, 2)
        self.assertEqual(self.pile.raw, self.pile.sorted)

    def test_counting_sort_base_16(self):
        digits = math.floor(math.log(max(self.pile.raw), 16)) + 1
        sorting.radix_sort(self.pile.raw, digits, 16)
        self.assertEqual(self.pile.raw, self.pile.sorted)


if __name__ == "__main__":
    unittest.main()
