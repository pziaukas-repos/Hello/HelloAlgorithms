import sorting
import tests
import unittest


class CountingSortTest(unittest.TestCase):
    def setUp(self):
        self.pile = tests.Pile(100)

    def test_counting_sort(self):
        self.assertEqual(sorting.counting_sort(self.pile.raw, max(self.pile.raw) + 1), self.pile.sorted)


if __name__ == "__main__":
    unittest.main()
