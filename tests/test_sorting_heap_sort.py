import sorting
import tests
import unittest


class HeapSortTest(unittest.TestCase):
    def setUp(self):
        self.pile = tests.Pile(100)

    def test_heap_properties(self):
        n = 6
        heap = sorting.Heap(list(range(n)))

        self.assertIsNone(heap.parent(0))
        self.assertEqual(heap.parent(1), 0)
        self.assertEqual(heap.parent(4), 1)

        self.assertIsNone(heap.left(3))
        self.assertEqual(heap.left(1), 3)
        self.assertEqual(heap.left(0), 1)

        self.assertIsNone(heap.right(4))
        self.assertIsNone(heap.right(2))
        self.assertEqual(heap.right(0), 2)

        self.assertEqual(heap.size, n)
        for k in range(n):
            self.assertEqual(heap[k], k)

    def test_max_heapify(self):
        heap = sorting.Heap([0, 5, 4, 3, 2, 1])
        sorting.max_heapify(heap, 0)
        self.assertEqual(heap.raw, [5, 3, 4, 0, 2, 1])

    def test_build_max_heap(self):
        heap = sorting.Heap([3, 4, 1, 6, 2, 8])
        sorting.build_max_heap(heap)
        self.assertEqual(heap.raw, [8, 6, 3, 4, 2, 1])

    def test_heap_sort(self):
        sorting.heap_sort(self.pile.raw)
        self.assertEqual(self.pile.raw, self.pile.sorted)


if __name__ == "__main__":
    unittest.main()
