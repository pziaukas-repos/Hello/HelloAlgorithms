"""
Quick sort
==============
Divides a large list into two smaller sublists: the low elements and the high elements.
It can then recursively sort the sublists.
"""
from random import randrange
from typing import List


def partition(seq: List[int], p: int, r: int, randomized: bool = False) -> int:
    """
    Rearranges seq[p .. r-1] into high ant low elements, returns pivot index.
    Runs in O(n) time.
    :param seq: An arbitrary list.
    :param p: Start index.
    :param r: End index (non-inclusive).
    :param randomized: Choose pivot value randomly.
    :return: Pivot index.
    """
    if randomized:
        q = randrange(p, r)
        seq[q], seq[r - 1] = seq[r - 1], seq[q]
    pivot = seq[r - 1]
    q = p - 1
    for k in range(p, r - 1):
        if seq[k] <= pivot:
            q += 1
            seq[q], seq[k] = seq[k], seq[q]
    seq[q + 1], seq[r - 1] = seq[r - 1], seq[q + 1]
    return q + 1


def quick_sort(seq: List[int], p: int = 0, r: int = None, randomized: bool = False) -> None:
    """
    Sorts the integer values using quick sort technique.
    Runs in O(n^2) time.
    :param seq: An arbitrary list.
    :param p: Start index.
    :param r: End index (non-inclusive).
    :param randomized: Randomize the input.
    :return: Returns nothing, sorts seq[p .. r-1] in place.
    """
    r = r or len(seq)
    if r - p > 1:
        q = partition(seq, p, r, randomized)
        quick_sort(seq, p, q, randomized)
        quick_sort(seq, q, r, randomized)
