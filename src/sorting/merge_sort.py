"""
Merge sort
==========
An efficient, general-purpose, comparison-based sorting algorithm.
It divides a list into n trivial sublists and merges them back into one sorted list.
"""
import math
from typing import List


def merge(seq: List[int], p: int, q: int, r: int) -> None:
    """
    Merges two sorted piles seq[p .. q-1] and seq[q .. r-1] into a sorted whole.
    Runs in O(n) time.
    :param seq: A list containing two sorted sublists.
    :param p: Start index of the first sorted sublist.
    :param q: Start index of the second sorted sublist.
    :param r: End index of the second sorted sublist (non-inclusive).
    :return: Returns nothing, the sublist seq[p .. r] is sorted in place.
    """
    l_seq = seq[p:q] + [math.inf]
    r_seq = seq[q:r] + [math.inf]
    for k in range(p, r):
        seq[k] = r_seq.pop(0) if r_seq[0] < l_seq[0] else l_seq.pop(0)


def merge_sort(seq: List[int], p: int = 0, r: int = None) -> None:
    """
    Sorts the integer values using merge sort technique.
    Runs in O(n log(n)) time.
    :param seq: An arbitrary list.
    :param p: Start index.
    :param r: End index (non-inclusive).
    :return: Returns nothing, sorts seq[p .. r-1] in place.
    """
    r = r or len(seq)
    if r - p > 1:
        q = (p + r) // 2
        merge_sort(seq, p, q)
        merge_sort(seq, q, r)
        merge(seq, p, q, r)
