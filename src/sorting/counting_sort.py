"""
Counting sort
==============
Operates by counting the number of objects that have each distinct key value.
Determines the positions of each key value in the output sequence using arithmetic on those counts.
"""
from typing import List


def counting_sort(seq: List[int], r: int) -> List[int]:
    """
    Sorts the integer values using counting sort technique.
    Runs in O(n + r) time.
    :param seq: An arbitrary list.
    :param r: Range of possible values.
    :returns: A sorted list.
    """
    seq_sorted = [None] * len(seq)
    counter = [0] * r
    # number of elements equal to its index
    for k in range(len(seq)):
        counter[seq[k]] += 1
    # number of elements less than or equal to its index
    for r_k in range(1, r):
        counter[r_k] += counter[r_k - 1]
    for k in reversed(range(len(seq))):
        seq_sorted[counter[seq[k]] - 1] = seq[k]
        counter[seq[k]] -= 1
    return seq_sorted
