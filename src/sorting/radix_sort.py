"""
Radix sort
==============
Sorts on a digit level.
Begins at the least significant digit, and proceeds to the most significant digit.
"""
from typing import List


def radix_sort(seq: List[int], d: int, r: int = 10) -> List[int]:
    """
    Sorts the integer values using radix sort technique.
    Runs in O(d(n + r)) time.
    :param seq: An arbitrary list.
    :param d: Number of digits.
    :param r: Range of a digit.
    """
    for k in range(d):
        # any stable sort, presumably runs in O(n + r) time
        seq.sort(key=lambda x, k=k, r=r: (x // r**k) % r)
