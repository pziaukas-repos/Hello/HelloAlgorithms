"""
Insertion sort
==============
A simple sorting algorithm that builds the final sorted list one item at a time.
"""
from typing import List


def insertion_sort(seq: List[int]) -> List[int]:
    """
    Sorts the integer values using insertion sort technique.
    Runs in O(n^2) time.
    :param seq: An arbitrary list.
    :returns: A sorted list.
    """
    for j in range(1, len(seq)):
        # insert seq[j] into the sorted sequence seq[0 .. j-1]
        key = seq[j]
        i = j
        while i and seq[i - 1] > key:
            seq[i] = seq[i - 1]
            i -= 1
        seq[i] = key
    return seq
