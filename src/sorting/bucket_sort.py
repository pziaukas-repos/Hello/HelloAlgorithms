"""
Bucket sort
==============
Assumes uniform distribution.
Places the elements of an array into a number of buckets. Each bucket is then sorted individually.
"""
import math
from typing import List


def bucket_sort(seq: List[int], k: int) -> List[int]:
    """
    Sorts the integer values using bucket sort technique.
    Runs in O(n + k) time on average.
    :param seq: An arbitrary list.
    :param k: Number of buckets.
    :returns: A sorted list.
    """
    maximum = max(seq)
    # assumes efficiently implemented structure, e.g. linked lists
    buckets = [[] for _ in range(k)]
    for x in seq:
        buckets[math.floor(x / (maximum + 1) * k)].append(x)
    for bucket in buckets:
        bucket.sort()
    seq_sorted = [x for bucket in buckets for x in bucket]
    return seq_sorted
