"""
Heap sort
=========
An improved selection sort.
It divides its input into a sorted and an unsorted region, and iteratively shrinks the unsorted region.
"""
from typing import List


class Heap:
    def __init__(self, seq: List[int]) -> None:
        """
        Constructs a binary heap data structure.
        :param seq: An arbitrary list.
        """
        self.raw = seq or []
        self.size = len(seq)

    def parent(self, n: int) -> int:
        """
        :param n: An index.
        :return: Returns index of a parent of Heap[n].
        """
        return None if n == 0 else (n + 1) // 2 - 1

    def left(self, n: int) -> int:
        """
        :param n: An index.
        :return: Returns index of a left child of Heap[n].
        """
        index = 2 * n + 1
        return index if 0 <= index < self.size else None

    def right(self, n: int) -> int:
        """
        :param n: An index.
        :return: Returns index of a right child of Heap[n].
        """
        index = 2 * n + 2
        return index if 0 <= index < self.size else None

    def __getitem__(self, n: int) -> int:
        """
        An item getter.
        :param n: An index to look for.
        :return: Returns Heap[n].
        """
        return self.raw[n]

    def __setitem__(self, n: int, value: int) -> None:
        """
        An item setter.
        :param n: An index to write to.
        :param value: A value to write.
        """
        self.raw[n] = value


def max_heapify(heap: Heap, k: int) -> None:
    """
    Let's the element at index k "float down" the sorted heap.
    Runs in O(log(n)) time.
    :param heap: A heap to work with.
    :param k: An index below which the heap is sorted.
    """
    left = heap.left(k)
    right = heap.right(k)
    largest = left if left and heap[left] > heap[k] else k
    largest = right if right and heap[right] > heap[largest] else largest
    if largest != k:
        heap[k], heap[largest] = heap[largest], heap[k]
        max_heapify(heap, largest)


def build_max_heap(heap: Heap) -> None:
    """
    Max-heapify all non-leaf nodes of a heap.
    Runs in O(n) time.
    :param heap: A heap to work with.
    """
    for k in reversed(range(heap.size // 2)):
        max_heapify(heap, k)


def heap_sort(seq: List[int]) -> None:
    """
    Sorts the integer values using heap sort technique.
    Runs in O(n log(n)) time.
    :param seq: An arbitrary sequence.
    """
    heap = Heap(seq)
    build_max_heap(heap)
    for k in reversed(range(1, len(seq))):
        heap[0], heap[k] = heap[k], heap[0]
        heap.size -= 1
        max_heapify(heap, 0)
