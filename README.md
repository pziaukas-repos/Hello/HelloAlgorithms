# Hello Algorithms

Python algorithms project, based on classical techniques.


## Sorting (comparison)


### Insertion sort

##### Description
A simple sorting algorithm that builds the final sorted list one item at a time.

![INSERTION SORT](img/insertion_sort.gif)

##### Properties
The number of values to sort is $`n`$.

| Case    | Time complexity |
| ------- |:---------------:|
| Best    | $`O(n)`$        |
| Average | $`O(n^2)`$      |
| Worst   | $`O(n^2)`$      |

* In-place: **yes**
* Stable: **yes**


### Merge sort

##### Description
An efficient, general-purpose, comparison-based sorting algorithm.
It divides a list into n trivial sublists and merges them back into one sorted list.

![MERGE SORT](img/merge_sort.gif)

##### Properties
The number of values to sort is $`n`$.

| Case    | Time complexity  |
| ------- |:----------------:|
| Best    | $`O(n \log(n))`$ |
| Average | $`O(n \log(n))`$ |
| Worst   | $`O(n \log(n))`$ |

* In-place: **no**
* Stable: **yes**


### Heap sort

##### Description
An improved selection sort.
It divides its input into a sorted and an unsorted region, and iteratively shrinks the unsorted region.

![HEAP SORT](img/heap_sort.gif)

##### Properties
The number of values to sort is $`n`$.

| Case    | Time complexity  |
| ------- |:----------------:|
| Best    | $`O(n \log(n))`$ |
| Average | $`O(n \log(n))`$ |
| Worst   | $`O(n \log(n))`$ |

* In-place: **yes**
* Stable: **no**


### Quick sort

##### Description
Divides a large list into two smaller sublists: the low elements and the high elements.
It can then recursively sort the sublists.

![QUICK SORT](img/quick_sort.gif)

##### Properties
The number of values to sort is $`n`$.

| Case    | Time complexity  |
| ------- |:----------------:|
| Best    | $`O(n \log(n))`$ |
| Average | $`O(n \log(n))`$ |
| Worst   | $`O(n^2)`$       |

* In-place: **yes**
* Stable: **no**



## Sorting (non-comparison)


### Counting sort

##### Description
Operates by counting the number of objects that have each distinct key value.
Determines the positions of each key value in the output sequence using arithmetic on those counts.

![COUNTING SORT](img/counting_sort.gif)

##### Properties
The number of integer values to sort is $`n`$.
The range of possible values is bounded by $`r`$.

| Case    | Time complexity |
| ------- |:---------------:|
| Best    | $`O(n + r)`$    |
| Average | $`O(n + r)`$    |
| Worst   | $`O(n + r)`$    |

* In-place: **no**
* Stable: **yes**


### Radix sort

##### Description
Sorts on a digit level.
Begins at the least significant digit, and proceeds to the most significant digit.

![RADIX SORT](img/radix_sort.gif)

##### Properties
The number of integer values to sort is $`n`$.
The number of digits (arbitrary base) is $`d`$.
The range of digit values is $`r`$ ($`10`$ by default).

| Case    | Time complexity |
| ------- |:---------------:|
| Average | $`O(d(n + r))`$ |
| Worst   | $`O(d(n + r))`$ |

* In-place: **yes**
* Stable: **yes**


### Bucket sort

##### Description
Assumes uniform distribution.
Places the elements of an array into a number of buckets. Each bucket is then sorted individually.

![BUCKET SORT](img/bucket_sort.png)

##### Properties
The number of integer values to sort is $`n`$.
The number of buckets is $`k`$.

| Case    | Time complexity                  |
| ------- |:--------------------------------:|
| Average | $`O(n + k)`$                     |
| Worst   | $`O(n \log(n))`$ (single bucket) |

* In-place: **no**
* Stable: **yes**
